const SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');
const XLSX = require('xlsx');
const fs = require('fs');

var newInput = [];
var input_new = [];
var sentenceHeaders = ["Title", "Start Time", "End Time", "Sentence No"];
var sentenceHeaders_New = ["Title", "Start Time", "End Time"];
var wordHeaders = ["Title", "Start Time", "End Time", "Word No"];

//const speech_to_text = new SpeechToTextV1({
    //username: "2fb12812-b34a-401f-a005-4d2ab2f72e6d",
    //password: "qbSut3LD8kb0"
//});
const speech_to_text = new SpeechToTextV1({ 
  iam_apikey:'14YBI9EowC0YPykzh1mDftO1SUgenTrpu6D0GVfSCpxT',
  url:'https://gateway-lon.watsonplatform.net/speech-to-text/api'
}); 

exports.createSmil = async function (req, res) {
    console.log(req.query);
    let dirPath = req.query.dir;
    console.log("dirPath*********", dirPath);
    fs.readdir(dirPath + '/audio/', function (err, files) {
        if (err) {
            return console.error(err);
        }
        files.forEach((file, index) => {
            let comPath = dirPath + '/audio/' + file;
            let params = {
                audio: fs.createReadStream(comPath),
                content_type: 'audio/mp3',
                timestamps: true
            };
            speech_to_text.recognize(params, function (error, transcript) {
                if (error) {
                    console.log('Error:', error);
                } else {
                    let fileData = new Array;
                    if (req.query.type == "sentence") {
                        newInput = [];
                        for (let i = 0; i < transcript.results.length; i++) {
                            for (let j = 0; j < transcript.results[i].alternatives[0].timestamps.length; j++) {
                                transcript.results[i].alternatives[0].timestamps[j].push(Number(i + 1));
                                newInput.push(transcript.results[i].alternatives[0].timestamps[j]);
                            }
                        }
                        let temp_new = [];
                        input_new = [];
                        for (let i = 0; i < transcript.results.length; i++) {
                            temp_new = []
                            temp_new.push(transcript.results[i].alternatives[0].transcript);
                            temp_new.push(transcript.results[i].alternatives[0].timestamps[0][1]);
                            temp_new.push(transcript.results[i].alternatives[0].timestamps[transcript.results[i].alternatives[0].timestamps.length - 1][2]);
                            input_new.push(temp_new);
                        }

                    } else if (req.query.type == "word") {
                        for (let i = 0; i < transcript.results.length; i++) {
                            let temp = transcript.results[i].alternatives[0].timestamps;
                            fileData.push(temp);
                        }
                        let inputData = fileData;
                        newInput = [];
                        for (let j = 0; j < inputData.length; j++) {
                            newInput = newInput.concat(inputData[j]);
                        }
                        for (let k = 0; k < newInput.length; k++) {
                            newInput[k].push(Number(k + 1));
                        }

                    }
                    if (req.query.type == "sentence") {
                        newInput.splice(0, 0, sentenceHeaders);
                        input_new.splice(0, 0, sentenceHeaders_New);

                    } else if (req.query.type == "word") {
                        newInput.splice(0, 0, wordHeaders);
                    }
                    let data = newInput;
                    let ws = XLSX.utils.aoa_to_sheet(data);
                    let wb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wb, ws, "audio_Cuepoints", );
                    let data1;
                    if (input_new.length > 0) {
                        data1 = input_new;
                        let ws1 = XLSX.utils.aoa_to_sheet(data1);
                        XLSX.utils.book_append_sheet(wb, ws1, "with_sentence");
                    }
                    let fileName = file;
                    file = dirPath + '/ibm/' + fileName.slice(0, -4) + '.xlsx';
                    XLSX.writeFile(wb, file, {
                        type: 'buffer',
                        bookType: "xlsx"
                    });
                }

                if (index == files.length - 1) {
                    res.json({
                        'res': "Successfully Done !!!"
                    })
                }
            });
        });
    });
}