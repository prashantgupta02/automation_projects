var express = require('express');
var app = express();
var router = express.Router();
var bodyParser = require('body-parser');

var aud = require('./extracting_audio_cue_points');
let timeout = 0;
router.get('/smil', function (req, res) {
    req.setTimeout(timeout);
    aud.createSmil(req, res)
});
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('', router);
var port = process.env.PORT || 3000;
app.listen(port);
app.timeout = -1;
console.log('Server started at =    => localhost/' + port);